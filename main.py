#!/usr/bin/env python

from gcp_start_stop.compute import Compute
import logging
from flask import abort


def main(request):

    debug = request.args.get('debug')
    if debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger(__name__)

    logger.debug('Request headers: {}'.format(request.headers))
    #
    # parse args
    #
    content_type = request.headers.get('content-type')
    logger.debug('Content type is: {}'.format(content_type))
    if content_type == 'application/x-www-form-urlencoded':
        logger.debug(request.form)
        # mandatory
        project = request.form.get('project')
        # get a list
        zones = request.form.get('zone', '').split(',')
        action = request.form.get('action')
        region = request.form.get('region')
        # optional
        pattern = request.form.get('pattern')
        dryrun = request.form.get('dryrun')
        autoscalers = request.form.get('autoscalers')
        autoscaler_pattern = request.form.get('autoscaler-pattern')
        migs = request.form.get('migs')
        mig_pattern = request.form.get('mig-pattern')
        wait = request.form.get('wait')
    else:
        logger.debug(request.args)
        project = request.args.get('project')
        # get a list
        zones = request.args.get('zone', '').split(',')
        action = request.args.get('action')
        region = request.args.get('region')
        # optional
        pattern = request.args.get('pattern')
        dryrun = request.args.get('dryrun')
        autoscalers = request.args.get('autoscalers')
        autoscaler_pattern = request.args.get('autoscaler-pattern')
        migs = request.args.get('migs')
        mig_pattern = request.args.get('mig-pattern')
        wait = request.args.get('wait')

    # mandatory
    if project is None:
        logger.debug("project is mandatory.")
        abort(400)
    if region is None:
        logger.debug("region is mandatory.")
        abort(400)
    if zones is None:
        logger.debug("zones is mandatory.")
        abort(400)
    if action is None:
        logger.debug("action is mandatory.")
        abort(400)

    #
    # main
    #
    compute = Compute(
        project=project,
        zones=zones,
        region=region,
        dryrun=dryrun,
        wait=wait
    )

    if action == 'list':
        compute.list(
            pattern=pattern,
            autoscalers=autoscalers,
            autoscaler_pattern=autoscaler_pattern,
            migs=migs,
            mig_pattern=mig_pattern
        )
    elif action == 'stop':
        compute.stop(
            pattern=pattern,
            autoscalers=autoscalers,
            autoscaler_pattern=autoscaler_pattern,
            migs=migs,
            mig_pattern=mig_pattern
        )
    elif action == 'start':
        compute.start(
            pattern=pattern,
            autoscalers=autoscalers,
            autoscaler_pattern=autoscaler_pattern,
            migs=migs,
            mig_pattern=mig_pattern
        )
    else:
        self.logger.warning('Not implemented action')

    return 'OK'
