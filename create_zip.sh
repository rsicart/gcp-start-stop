#!/usr/bin/env bash

rm gcp-start-stop.zip && zip -r gcp-start-stop.zip main.py requirements.txt gcp_start_stop/*.py
