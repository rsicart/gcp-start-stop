import googleapiclient.discovery
import re
import time
import logging


class Compute:

    def __init__(self, project=None, zones=None, region=None, dryrun=None, wait=None):
        # A null handler ignores all logging messages by default. Thus, if the
        # library is used and logging is never configured, no messages or
        # warnings will appear
        self.logger = logging.getLogger(__name__)
        self.logger.addHandler(logging.NullHandler())

        self.compute = googleapiclient.discovery.build(
            'compute', 'v1',
            cache_discovery=False
        )
        self.project = project
        self.zones = zones
        self.region = region
        self.dryrun = dryrun
        self.wait = wait

    def get_instances(self, pattern, status):
        result = []
        filter_list = [
            '(status = "{}")'.format(status)
        ]
        for zone in self.zones:
            result_zone = self.compute.instances().list(
                project=self.project,
                zone=zone,
                filter=' '.join(filter_list),
            ).execute()
            result.extend(
                result_zone['items'] if 'items' in result_zone else []
            )
        if pattern:
            filtered = [
                instance for instance in result
                if re.search(pattern, instance['name'])
            ]
            return filtered
        return result

    def get_zone_from_url(self, zone_url):
        # https://www.googleapis.com/compute/v1/projects/my-project-name/zones/europe-west1-b
        return zone_url.split('/')[-1]

    def get_region_from_url(self, region_url):
        # https://www.googleapis.com/compute/v1/projects/my-project-name/regions/europe-west1
        return region_url.split('/')[-1]

    def stop_instance(self, instance):
        instance_zone = self.get_zone_from_url(instance['zone'])
        instance_name = instance['name']

        if not self.dryrun:
            self.logger.info(
                "Stopping instance %s in zone %s" %
                (instance_name, instance_zone)
            )
            return self.compute.instances().stop(
                project=self.project,
                zone=instance_zone,
                instance=instance_name).execute()
        else:
            self.logger.info(
                "(Dry Run) Stopping instance %s in zone %s" %
                (instance_name, instance_zone)
            )
            operation = {'name': 'stop instance dry run'}
            return operation

    def start_instance(self, instance):
        instance_zone = self.get_zone_from_url(instance['zone'])
        instance_name = instance['name']

        if not self.dryrun:
            self.logger.info(
                "Starting instance %s in zone %s" %
                (instance_name, instance_zone)
            )
            return self.compute.instances().start(
                project=self.project,
                zone=instance_zone,
                instance=instance_name).execute()
        else:
            self.logger.info(
                "(Dry Run) Starting instance %s in zone %s" %
                (instance_name, instance_zone)
            )
            operation = {'name': 'start instance dry run'}
            return operation

    def wait_for_operation(self, operation, zone):
        if not self.dryrun:
            self.logger.info('Waiting for operation to finish...')
            while True:
                result = self.compute.zoneOperations().get(
                    project=self.project,
                    zone=zone,
                    operation=operation).execute()

                if result['status'] == 'DONE':
                    self.logger.info("done.")
                    if 'error' in result:
                        raise Exception(result['error'])
                    return result

                time.sleep(1)
        else:
            self.logger.info('(Dry run) Waiting for operation to finish...')
            self.logger.info('(Dry run) Operation done...')

    def wait_for_region_operation(self, operation, region):
        if not self.dryrun:
            self.logger.info('Waiting for operation to finish...')
            while True:
                result = self.compute.regionOperations().get(
                    project=self.project,
                    region=region,
                    operation=operation).execute()

                if result['status'] == 'DONE':
                    self.logger.info("done.")
                    if 'error' in result:
                        raise Exception(result['error'])
                    return result

                time.sleep(1)
        else:
            self.logger.info('(Dry run) Waiting for operation to finish...')
            self.logger.info('(Dry run) Operation done...')

    def get_autoscalers(self, pattern):
        result = []
        filter_list = [
            '(status = "ACTIVE")',
        ]

        result_region = self.compute.regionAutoscalers().list(
            project=self.project,
            region=self.region,
            filter=' '.join(filter_list),
        ).execute()
        result.extend(
            result_region['items']
            if 'items' in result_region else []
        )
        if pattern:
            filtered = [
                autoscaler for autoscaler in result
                if re.search(pattern, autoscaler['name'])
            ]
            return filtered
        return result

    def stop_autoscaler(self, autoscaler):
        autoscaler_region = self.get_region_from_url(autoscaler['region'])
        autoscaler_name = autoscaler['name']

        autoscaling_policy = autoscaler.get('autoscalingPolicy')
        autoscaling_policy['mode'] = 'OFF'
        patch_policy = {'autoscalingPolicy': autoscaling_policy}

        if not self.dryrun:
            self.logger.info(
                "Stopping autoscaler %s in region %s" %
                (autoscaler_name, autoscaler_region)
            )
            self.logger.info(
                ("Stopping autoscaler %s in region %s, "
                 "with autoscaling policy %s") %
                (autoscaler_name, autoscaler_region, autoscaling_policy)
            )
            return self.compute.regionAutoscalers().patch(
                project=self.project,
                region=autoscaler_region,
                autoscaler=autoscaler_name, body=patch_policy).execute()
        else:
            self.logger.info(
                ("(Dry run) Stopping autoscaler %s in region %s, "
                 "with autoscaling policy %s") %
                (autoscaler_name, autoscaler_region, autoscaling_policy)
            )
            operation = {'name': 'stop autoscaler dry run'}
            return operation

    def start_autoscaler(self, autoscaler):
        autoscaler_region = self.get_region_from_url(autoscaler['region'])
        autoscaler_name = autoscaler['name']

        autoscaling_policy = autoscaler.get('autoscalingPolicy')
        autoscaling_policy['mode'] = 'ON'
        patch_policy = {'autoscalingPolicy': autoscaling_policy}

        if not self.dryrun:
            self.logger.info(
                "Starting autoscaler %s in region %s" %
                (autoscaler_name, autoscaler_region)
            )
            self.logger.info(
                ("Stopping autoscaler %s in region %s, "
                 "with autoscaling policy %s") %
                (autoscaler_name, autoscaler_region, autoscaling_policy)
            )
            return self.compute.regionAutoscalers().patch(
                project=self.project,
                region=autoscaler_region,
                autoscaler=autoscaler_name, body=patch_policy).execute()
        else:
            self.logger.info(
                ("(Dry run) Starting autoscaler %s in region %s, "
                 "with autoscaling policy %s") %
                (autoscaler_name, autoscaler_region, autoscaling_policy)
            )
            operation = {'name': 'start autoscaler dry run'}
            return operation

    def get_migs(self, pattern):
        result = []
        result_region = self.compute.regionInstanceGroupManagers().list(
            project=self.project,
            region=self.region,
        ).execute()
        result.extend(
            result_region['items'] if 'items' in result_region else []
        )
        if pattern:
            filtered = [
                mig for mig in result if re.search(pattern, mig['name'])
            ]
            return filtered
        return result

    def resize_mig(self, mig, size):
        mig_region = self.get_region_from_url(mig['region'])
        mig_name = mig['name']

        if not self.dryrun:
            self.logger.info(
                "Resizing mig %s to size %s in region %s" %
                (mig_name, size, mig_region)
            )
            return self.compute.regionInstanceGroupManagers().resize(
                project=self.project,
                region=mig_region,
                instanceGroupManager=mig_name, size=size).execute()
        else:
            self.logger.info(
                "(Dry run) Resizing mig %s to size %s in region %s" %
                (mig_name, size, mig_region)
            )
            operation = {'name': 'resize mig dry run'}
            return operation

    def list(self, pattern=None, autoscalers=None,
             autoscaler_pattern=None, migs=None, mig_pattern=None):
        instances = self.get_instances(pattern, 'Running')
        self.logger.info('Listing instances.')
        for instance in instances:
            self.logger.info(' - ' + instance['name'])

        autoscalers = self.get_autoscalers(autoscaler_pattern)
        self.logger.info('Listing autoscalers.')
        for autoscaler in autoscalers:
            self.logger.info(' - ' + autoscaler['name'])

        migs = self.get_migs(mig_pattern)
        self.logger.info('Listing regional migs.')
        for mig in migs:
            self.logger.info(' - ' + mig['name'])

    def stop(self, pattern=None, autoscalers=None,
             autoscaler_pattern=None, migs=None, mig_pattern=None):
        instances = self.get_instances(pattern, 'Running')
        for instance in instances:
            operation = self.stop_instance(instance)
            zone = self.get_zone_from_url(instance['zone'])
            if self.wait:
                self.wait_for_operation(operation['name'], zone)

        autoscalers = self.get_autoscalers(autoscaler_pattern)
        for autoscaler in autoscalers:
            operation = self.stop_autoscaler(autoscaler)
            region = self.get_region_from_url(autoscaler['region'])
            if self.wait:
                self.wait_for_region_operation(operation['name'], region)

        migs = self.get_migs(mig_pattern)
        for mig in migs:
            operation = self.resize_mig(mig, 0)
            region = self.get_region_from_url(mig['region'])
            if self.wait:
                self.wait_for_region_operation(operation['name'], region)

    def start(self, pattern=None, autoscalers=None,
             autoscaler_pattern=None, migs=None, mig_pattern=None):
        instances = self.get_instances(pattern, 'Terminated')
        for instance in instances:
            operation = self.start_instance(instance)
            zone = self.get_zone_from_url(instance['zone'])
            if self.wait:
                self.wait_for_operation(operation['name'], zone)

        autoscalers = self.get_autoscalers(autoscaler_pattern)
        for autoscaler in autoscalers:
            operation = self.start_autoscaler(autoscaler)
            region = self.get_region_from_url(autoscaler['region'])
            if self.wait:
                self.wait_for_region_operation(operation['name'], region)

        self.logger.info('MIG resize will be performed by autoscaler.')

