#!/usr/bin/env python

import argparse
from gcp_start_stop.compute import Compute
import logging


def main(project=None, zones=None, region=None, dryrun=None, action=None,
         pattern=None, autoscalers=None, autoscaler_pattern=None, migs=None,
         mig_pattern=None, wait=None):

    logger = logging.getLogger(__name__)

    compute = Compute(
        project=project,
        zones=zones,
        region=region,
        dryrun=dryrun,
        wait=wait
    )

    if action == 'list':
        compute.list(
            pattern=pattern,
            autoscalers=autoscalers,
            autoscaler_pattern=autoscaler_pattern,
            migs=migs,
            mig_pattern=mig_pattern
        )
    elif action == 'stop':
        compute.stop(
            pattern=pattern,
            autoscalers=autoscalers,
            autoscaler_pattern=autoscaler_pattern,
            migs=migs,
            mig_pattern=mig_pattern
        )
    elif action == 'start':
        compute.start(
            pattern=pattern,
            autoscalers=autoscalers,
            autoscaler_pattern=autoscaler_pattern,
            migs=migs,
            mig_pattern=mig_pattern
        )
    else:
        self.logger.warning('Not implemented action')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('project_id', help='Your Google Cloud project ID.')
    parser.add_argument(
        '--zone',
        required=True,
        action='append',
        choices=['europe-west1-b', 'europe-west1-c', 'europe-west1-d'],
        help=(
            'Compute Engine zone. '
            'Add multiple zones by adding more --zone arguments.'
        )
    )
    parser.add_argument(
        '--action',
        required=True,
        choices=['stop', 'start', 'list'],
        help='Action to perform.'
    )
    parser.add_argument(
        '--pattern',
        help='Filter instance names matching this pattern.'
    )
    parser.add_argument(
        '--dryrun',
        action='store_true',
        help='Do not execute real actions, just simulate them.'
    )
    parser.add_argument(
        '--debug',
        action='store_true',
        help='Setup loglevel to debug.'
    )
    parser.add_argument(
        '--autoscalers',
        action='store_true',
        help='If set, update autoscalers.'
    )
    parser.add_argument(
        '--autoscaler-pattern',
        help='Filter autoscaler names matching this pattern.'
    )
    parser.add_argument(
        '--region',
        required=True,
        choices=['europe-west1'],
        help='Compute Engine region.'
    )
    parser.add_argument(
        '--migs',
        action='store_true',
        help='If set, update migs.'
    )
    parser.add_argument(
        '--mig-pattern',
        help='Filter mig names matching this pattern.'
    )
    parser.add_argument(
        '--wait',
        action='store_true',
        help='Wait for operations to finish.'
    )

    args = parser.parse_args()

    if args.debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)


main(
    project=args.project_id,
    zones=args.zone,
    region=args.region,
    dryrun=args.dryrun,
    action=args.action,
    pattern=args.pattern,
    autoscalers=args.autoscalers,
    autoscaler_pattern=args.autoscaler_pattern,
    migs=args.migs,
    mig_pattern=args.mig_pattern,
    wait=args.wait
)
